/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import { RootNavigator } from '@navigation';

const App = () => {
  return (
    <RootNavigator />
  );
};

export default App;
