import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StyleSheet } from 'react-native';
import { SplashComponent, LedController} from '@screens';

const Stack = createStackNavigator();

export const RootNavigator = () => {
    return(
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Splash"
          component={SplashComponent}
          options={{
            title: 'Splash',
            headerStyle: { backgroundColor: '#fff', elevation: 0, shadowOpacity: 0, shadowColor: 'transparent', borderBottomWidth: 0 },
            headerShown: false
          }}
        />
        <Stack.Screen
          name="LedController"
          component={LedController}
          options={{ title: 'Led Controller', headerShown: false }}
        />
       
      </Stack.Navigator>
    </NavigationContainer >
  )};

const styles = StyleSheet.create({  
  nav: {
    height: 500
  }
})