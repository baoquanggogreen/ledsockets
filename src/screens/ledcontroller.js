import React, { Component } from 'react';
import { StyleSheet, 
         Text, 
         View, 
         DeviceEventEmitter,
         TouchableOpacity,
         Image
       } from 'react-native';
import Sockets from 'react-native-sockets';
import { BACKGROUND_LED } from '@assets';

const config = {
  address: "192.168.1.14", //ip address of server
  port: 80, //port of socket server
  reconnect:true, //OPTIONAL (default false): auto-reconnect on lost server
  reconnectDelay:500, //OPTIONAL (default 500ms): how often to try to auto-reconnect
  maxReconnectAttempts:10, //OPTIONAL (default infinity): how many time to attemp to auto-reconnect
}

on = () => {
  Sockets.write("on\n");
  console.log("on");
}

off = () => {
  Sockets.write("off\n");
  console.log("off");
}

mode1 = () => {
  Sockets.write("mode1\n");
  console.log("mode1");
}

mode2 = () => {
  Sockets.write("mode2\n");
  console.log("mode2");
}

export class LedController extends Component {

  render() {
    Sockets.startClient(config);
    DeviceEventEmitter.addListener('socketClient_connected', () => {
      console.log('socketClient_connected');
    });
    return (
      <View style={styles.container}> 
      <Image source={BACKGROUND_LED} style={styles.backgroundImage} />
        <View style={styles.viewBtn}>
          <TouchableOpacity onPress={ on } style={styles.button}>
            <Text style={styles.text}>ON</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={ off } style={styles.button}>
            <Text style={styles.text}>OFF</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={ mode1 } style={styles.button}>
            <Text style={styles.text}>Mode 1</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={ mode2 } style={styles.button}>
            <Text style={styles.text}>Mode 2</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  viewBtn: {
    flexDirection: 'row',
    justifyContent: 'center',
    flexWrap: 'wrap'
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 120,
    height: 120,
    margin: 10,
    borderRadius: 5,
    backgroundColor: '#ff8c00',
    borderRadius: 60,
    elevation: 15
  },
  text: {
    color: '#fff',
    fontSize: 20
  },
  backgroundImage: {
    position: 'absolute',
    width: '100%',
    height: '100%'
  }
}); 